# Mixly-RFID智能门禁

一、实验目的及要求
熟悉RFID设备及操作。

二、实验原理与内容
1.读取校园卡ID号。
2.读取到指定校园卡使用S90舵机开门，并延时3秒后自动关闭。


三、实验软硬件环境
硬件：Arduino、RC522读卡器、校园卡
软件：Mixly IDE
